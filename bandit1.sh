#!/bin/bash

# Instala sshpass si no está instalado
if ! command -v sshpass &> /dev/null
then
    echo "sshpass no está instalado. Instalándolo ahora..."
    sudo apt-get install sshpass
fi

# Dirección del servidor
SERVER='bandit.labs.overthewire.org'

# Nombre de usuario y contraseña para el nivel 0
USER='bandit0'
PASSWORD='bandit0'

sshpass -p $PASSWORD ssh $USER@$SERVER $CMD -p 2220 <<EOF
    echo "the Key is"
    cat readme
    exit
EOF
# Comando para obtener la contraseña del siguiente nivel
#CMD='cat readme'
#echo $CMD
# Ejecuta el comando en el servidor a través de SSH


