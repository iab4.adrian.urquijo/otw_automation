#!/bin/bash

# Instala sshpass si no está instalado
if ! command -v sshpass &> /dev/null
then
    echo "sshpass no está instalado. Instalándolo ahora..."
    sudo apt-get install sshpass
fi

# Dirección del servidor
SERVER='bandit.labs.overthewire.org'
# LEVEL 0
# Nombre de usuario y contraseña para el nivel 0
USER='bandit0'
PASSWORD='bandit0'

sshpass -p $PASSWORD ssh $USER@$SERVER $CMD -p 2220 <<EOF
    echo "the Key is"
    cat readme
    exit
EOF
# LEVEL 1
USER='bandit1'
PASSWORD='NH2SXQwcBdpmTEzi3bvBHMM9H66vVXjL'

sshpass -p $PASSWORD ssh $USER@$SERVER $CMD -p 2220 <<EOF
    echo "the Key is"
    cat ./-
    exit
EOF
# LEVEL 2
USER='bandit2'
PASSWORD='rRGizSaX8Mk1RTb1CNQoXTcYZWU6lgzi'

sshpass -p $PASSWORD ssh $USER@$SERVER $CMD -p 2220 <<EOF
    echo "the Key is"
    cat spaces\ in\ this\ filename
    exit
EOF
# LEVEL 3
USER='bandit3'
PASSWORD='aBZ0W5EmUfAf7kHTQeOwd8bauFJ2lAiG'

sshpass -p $PASSWORD ssh $USER@$SERVER $CMD -p 2220 <<EOF
    echo "the Key is"
    cat inhere/.hidden
    exit
EOF
# LEVEL 4
USER='bandit4'
PASSWORD='2EW7BBsr6aMMoJ2HjW067dm8EgX26xNe'

sshpass -p $PASSWORD ssh $USER@$SERVER $CMD -p 2220 <<EOF
    echo "the Key is"
    cat ./inhere/-file07
    exit
EOF
# LEVEL 5
USER='bandit5'
PASSWORD='lrIWWI6bB37kxfiCQZqUdOIYfr6eEeqR'

sshpass -p $PASSWORD ssh $USER@$SERVER $CMD -p 2220 <<EOF
    echo "the Key is"
    cat ./inhere/maybehere07/.file2
    exit
EOF
# LEVEL 6
USER='bandit6'
PASSWORD='P4L4vucdmLnm8I7Vl7jG1ApGSfjYKqJU'

sshpass -p $PASSWORD ssh $USER@$SERVER $CMD -p 2220 <<EOF
    echo "the Key is"
    find / -user bandit7 -group bandit6 -size 33c 2>/dev/null | xargs cat
    exit
EOF
# LEVEL 7
USER='bandit7'
PASSWORD='z7WtoNQU2XfjmMtWA8u5rN4vzqu4v99S'

sshpass -p $PASSWORD ssh $USER@$SERVER $CMD -p 2220 <<EOF
    echo "the Key is"
    grep millionth data.txt | cut -f2
    exit
EOF